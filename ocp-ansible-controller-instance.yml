---
- name: Create Automation Controller admin password secret
  kubernetes.core.k8s:
    definition:
      api_version: v1
      metadata:
        name: controller-admin-password
        namespace: "{{ demo_namespace }}"
        labels:
          app.kubernetes.io/part-of: controller
      kind: Secret
      data:
        password: "{{ ansible_controller_instance_admin_password | b64encode }}"
    state: present
 
- name: Deploy Automation Controller instance
  kubernetes.core.k8s:
    state: present
    template:
      - path: "{{ ansible_controller_instance_templates_path }}/ansible-controller-instance.yml.j2"
 
- name: Wait till Automation Controller instance is fully deployed
  kubernetes.core.k8s_info:
    api_version: automationcontroller.ansible.com/v1beta1
    kind: AutomationController
    name: controller
    namespace: "{{ demo_namespace }}"
  register: aapctl_instance_obj
  until:
    - aapctl_instance_obj.resources[0].status |
       json_query('conditions[?type==`Successful`].status') == ["True"]
    - aapctl_instance_obj.resources[0].status |
       json_query('conditions[?type==`Running`].status') == ["True"]
  retries: 85
  delay: 10

- name: Wait till Automation Controller URL is reachable
  ansible.builtin.uri:
    url: "https://controller.{{ ocp_ingress_domain[0] }}"
    method: GET
    follow_redirects: none
    validate_certs: false
  register: _result
  until: _result.status == 200
  retries: 720 
  delay: 5

- name: Copy Automation Controller manifest file
  ansible.builtin.template:
    dest: /root/ansible-manifest.zip
    group: root
    src: "{{ ansible_controller_instance_templates_path }}/ansible-controller-manifest.zip.j2"
    mode: 0644
    owner: root

- name: Load Automation Controller manifest into variable
  ansible.builtin.slurp:
    src: /root/ansible-manifest.zip
  register: aapctl_manifest_file

- name: Post Automation Controller manifest file
  ansible.builtin.uri:
    url: "https://controller.{{ ocp_ingress_domain[0] }}/api/v2/config/"
    method: POST
    user: admin
    password: "{{ ansible_controller_instance_admin_password }}"
    body: >
      { 
       "eula_accepted": true, 
       "manifest": "{{ aapctl_manifest_file.content }}" 
      }
    body_format: json
    validate_certs: false
    force_basic_auth: true