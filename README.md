# demo-environment

Application Monitoring Traffic Demo

## Demo/Lab Developers:

*Carlos Lopez Bartolome*, Specialist Solution Architect, Red Hat

*Cesar Fernandez*, EMEA Ansible Specialist Solution Architect, Red Hat

## Prerequisites

- ServiceNow instance.
- OpenShift Cluster +4.12 with admin rights.
- Ansible Navigator.
- Demo Environment project.

### ServiceNow

You can request a ServiceNow Developer Instance in the following [link](https://developer.servicenow.com/).

### OpenShift

If you don't have an OpenShift Cluster 4.13 with admin rights, you can request one in the following [link](https://demo.redhat.com/catalog?category=Workshops&item=babylon-catalog-prod%2Fsandboxes-gpte.ocp413-wksp.prod).

### Ansible Navigator

- If you have Linux, you can install the `ansible-navigator` tool following the steps described in the following [link](https://ansible-navigator.readthedocs.io/installation/#linux).

> Note: Run the command `ansible-navigator --version` to make sure it was installed correctly.

- After installing it, create the `inventory` and the `ansible-navigator` definition file:

```sh
cd ~
mkdir ansible-navigator
cat << EOF > ansible-navigator/inventory
[controller]
localhost ansible_connection=local
EOF
cat << EOF > ansible-navigator/ansible-navigator.yml
---
ansible-navigator:
  ansible:
    inventory:
      entries:
      - ./inventory
  app: run
  editor:
    command: vim_from_setting
    console: false
  execution-environment:
    container-engine: podman
    image: quay.io/ansible_eda/ansible-eda-ee:1.0
    pull:
      policy: missing
  logging:
    append: true
    file: /tmp/navigator/ansible-navigator.log
    level: debug
  playbook-artifact:
    enable: false
EOF
```

## How to deploy the demo environment

### Demo Environment Project

- Clone the demo environment project:

```sh
cd ~
git clone https://gitlab.com/app-traffic-monitoring-demo/demo-environment.git
```

### Deploy Demo Environment

- Create the demo configuration variables file, replacing the highlighted variables:

```sh
cat << EOF > demo-environment/vars/demo-config.yml
---
# Demo vars
demo_namespace: ansible-eda-demo
demo_templates_path: templates

# ServiceNow vars
snow_instance_host: "*<SNOW_HOST>*"
snow_instance_username: "*<SNOW_USERNAME>*"
snow_instance_password: "*<SNOW_PASSWORD>*"
snow_eda_username: "ansible-eda"
snow_eda_password: "RedHat!123"

# OCP vars
ocp_templates_path: "{{ demo_templates_path }}/images"

# Gitea vars
gitea_templates_path: "{{ demo_templates_path }}/gitea"

# Ansible EDA vars
eda_templates_path: "{{ demo_templates_path }}/ansible-eda"

# ArgoCD vars
argocd_operator_templates_path: "{{ demo_templates_path }}/argocd/operator"
argocd_instance_templates_path: "{{ demo_templates_path }}/argocd/instance"
argocd_instance_admin_pasword: redhat
argocd_config_templates_path: "{{ demo_templates_path }}/argocd/config"

# Ansible vars
ansible_operator_templates_path: "{{ demo_templates_path }}/ansible/operator"
ansible_instance_templates_path: "{{ demo_templates_path }}/ansible/instance"
ansible_instance_admin_password: redhat
ansible_instance_manifest: "*<AAP_MANIFEST>*" --> !!!ENCODE IN BASE64!!!
```

- Deploy the demo environment:

```sh
cd ~/ansible-navigator
ansible-navigator run ../demo-environment/ocp-demo-deploy.yml -m stdout \
  -e 'ansible_python_interpreter=/usr/bin/python3' \
  -e 'openshift_api=<OPENSHIFT_URL>' \
  -e 'openshift_token=<OPENSHIFT_API>' \
  -e 'openshift_storage_class=gp3-csi'
```
